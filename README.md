Point Cloud Transformation
===============

Package to transform the coordinate frame of the point cloud from the camera link to the base of your robot. This code is adapted from the [pt_cloud_lab](http://correll.cs.colorado.edu/?p=2807)

## TO RUN TRASNFORM and SEGMENT of IMAGES
* Basic tutorial on point cloud transformation can be found [here](http://pointclouds.org/documentation/tutorials/matrix_transform.php)
* Build the package, i.e. `catkin build` at the root of your workspace
* Ideally export your robot `ROS_MASTER_URI`. At this stage, in the point_cloud_transformation.cpp file substitute the `arm_link` parameter with the base link of your arm. This is to be added in the reconfigurable in the future
* `roslaunch point_cloud_transformation point_cloud_transformation`
* You will see the rviz with the online transformed and segmented pc using the `/point_cloud_transformation/transformed_pc` topic
* If the segmentation is not accurate, play with tunning the parameters in the rqt_reconfigure gui:
 - meank and stdDev are dedicated to the statistical filter to remove background noise if required
 - limit_<> take care of the bounding box around the object
 - inlier_points, radians_threshold and cm_distance_thr removes the planar background
