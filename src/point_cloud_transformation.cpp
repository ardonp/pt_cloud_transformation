/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, University of Colorado, Boulder
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Univ of CO, Boulder nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Dave Coleman
   Desc:
*/
/*
  DEV NOTES:
  Types of clouds:
  cloud
  cloud_transformed
  cloud_filtered
  cloud_plane

*/

#include <ros/ros.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/Pose.h>
#include <sensor_msgs/PointCloud2.h>

#include <tf/transform_listener.h>

#include <pcl/ros/conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/passthrough.h>
// #include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/extract_indices.h>

#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/radius_outlier_removal.h>

#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

// specific for plane extraction
// #include <pcl/ModelCoefficients.h>
// #include <pcl/io/pcd_io.h>
// #include <pcl/point_types.h>
// #include <pcl/segmentation/organized_multi_plane_segmentation.h>
// #include <pcl/features/integral_image_normal.h>
// #include <pcl/common/time.h>

#include "scene_segmentation.hpp"
#include <pcl/common/centroid.h>
// --------------

#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <cmath>
#include <algorithm>

// Adding the dynamic reconfigure include
#include <dynamic_reconfigure/server.h>
#include <point_cloud_transformation/point_cloud_transformationConfig.h>
int meanK;
double stdDev;
double limit_min_z, limit_max_z, limit_min_x, limit_max_x, limit_min_y, limit_max_y;
int inlier_points;
double radians_threshold ,cm_distance_thr;

typedef pcl::PointXYZRGB PointT;

std::vector<Plane> detected_planes;

SceneSegmentation::SceneSegmentation() :
  voxel_resolution(0.0075f),
  seed_resolution(0.03f),
  color_importance(0.0f),
  spatial_importance(1.0),
  normal_importance(4.0f),
  use_single_cam_transform(false),
  use_supervoxel_refinement(false),
  concavity_tolerance_threshold(10),
  smoothness_threshold(0.1),
  min_segment_size(0),
  use_extended_convexity(false),
  use_sanity_criterion(false),
  k_factor(0),
  th_min_points_in_obj(100),
  th_max_points_in_obj(20000)
  {
    // input_cloud_ptr = pcl::PointCloud<PointT>::ConstPtr(new pcl::PointCloud<PointT>);
    sv_centroid_normal_cloud = pcl::PointCloud<pcl::PointNormal>::Ptr(new pcl::PointCloud<pcl::PointNormal>);
    sv_labeled_cloud = pcl::PointCloud<pcl::PointXYZL>::Ptr(new pcl::PointCloud<pcl::PointXYZL>);
    lccp_labeled_cloud = pcl::PointCloud<pcl::PointXYZL>::Ptr(new pcl::PointCloud<pcl::PointXYZL>);
  };

void SceneSegmentation::extractPlanes(int inlier_points, double radians_threshold, double cm_distance_thr, pcl::PointCloud<pcl::PointXYZRGB>::Ptr input_cloud_ptr) {
  std::cout << "Extracting planes..." << std::endl;

  // estimate normals
  pcl::IntegralImageNormalEstimation<PointT, pcl::Normal> ne;
  pcl::PointCloud<pcl::Normal>::Ptr normal_cloud (new pcl::PointCloud<pcl::Normal>);
  ne.setMaxDepthChangeFactor (0.03f);
  ne.setNormalSmoothingSize (20.0f);
  ne.setNormalEstimationMethod (ne.COVARIANCE_MATRIX);
  ne.setInputCloud (input_cloud_ptr);
  ne.compute (*normal_cloud);

  // segment planes
  double mps_start = pcl::getTime ();
  pcl::OrganizedMultiPlaneSegmentation<PointT, pcl::Normal, pcl::Label> mps;
  std::vector<pcl::PlanarRegion<PointT>, Eigen::aligned_allocator<pcl::PlanarRegion<PointT> > > regions;
  std::vector<pcl::ModelCoefficients> model_coefficients;
  std::vector<pcl::PointIndices> inlier_indices;
  pcl::PointCloud<pcl::Label>::Ptr labels (new pcl::PointCloud<pcl::Label>);
  std::vector<pcl::PointIndices> label_indices;
  std::vector<pcl::PointIndices> boundary_indices;

  mps.setMinInliers (inlier_points); //50000
  mps.setAngularThreshold (radians_threshold); //3 degrees
  // mps.setAngularThreshold (0.017453 * 2.0); //3 degrees
  mps.setDistanceThreshold (cm_distance_thr); //2cm = 0.02
  mps.setInputNormals (normal_cloud);
  mps.setInputCloud (input_cloud_ptr);
  mps.segmentAndRefine (regions, model_coefficients, inlier_indices, labels, label_indices, boundary_indices);
  double mps_end = pcl::getTime ();

  // get data from the planes
  std::cout << "  - radians_threshold: " << radians_threshold << std::endl;
  std::cout << "  - planes detected: " << regions.size () << std::endl;
  std::vector<int> idx;
  detected_planes.resize(regions.size());
  for (size_t i = 0; i < inlier_indices.size(); ++i) {
    pcl::PointIndices::Ptr plane_indices(new pcl::PointIndices);
    *plane_indices = inlier_indices[i];
    idx.insert(idx.end(), plane_indices->indices.begin(), plane_indices->indices.end());

    pcl::PointCloud<PointT>::Ptr contour (new pcl::PointCloud<PointT>);
    contour->points = regions[i].getContour();
    detected_planes[i].contour = contour;
  }

  // remove planes from main cloud
  pcl::PointIndices::Ptr overall_indices(new pcl::PointIndices);
  overall_indices->indices = idx;
  pcl::PointCloud<PointT>::Ptr tmp_cloud(new pcl::PointCloud<PointT>);
  pcl::ExtractIndices<PointT> extract;
  extract.setInputCloud(input_cloud_ptr);
  extract.setIndices(overall_indices);
  extract.setNegative(true);
  extract.filter(*tmp_cloud);
  input_cloud_ptr = tmp_cloud;
  // pcl::io::savePCDFile("test_pao_desk.pcd", *input_cloud_ptr);
}


namespace point_cloud_transformation
{

class PointCloudTrans
{
private:

  ros::NodeHandle nh_;
  std::string action_name_;

  ros::Subscriber point_cloud_sub_;
  ros::Publisher filtered_pub_; // filtered point cloud for testing the algorithms
  ros::Publisher plane_pub_; // points that were recognized as part of the table
  ros::Publisher block_pose_pub_; // publishes to the block logic server
  ros::Publisher transformed_pub_; // publishes to the block logic server
  tf::TransformListener tf_listener_;

  //std::vector<geometry_msgs::Pose> block_poses_;
  geometry_msgs::PoseArray block_poses_;

  // Parameters of problem
  std::string arm_link;
  double block_size;
  double table_height;


public:
  SceneSegmentation scene_segmentation;

  PointCloudTrans(const std::string name) :
    nh_("~"),
    action_name_(name)
  {
    ROS_INFO_STREAM("Starting Point Cloud Transformation node 3");


    // Parameters
    arm_link = "/base_link";
    block_size = 0.04;
    table_height = 0.0;
    block_poses_.header.stamp = ros::Time::now();
    block_poses_.header.frame_id = arm_link;


    // Subscribe to point cloud
    // point_cloud_sub_ = nh_.subscribe("/kinect2/hd/points", 1, &PointCloudTrans::cloudCallback, this); // for realsense PointCloud2 message: /camera/depth/color/points
    point_cloud_sub_ = nh_.subscribe("/camera/depth/color/points", 1, &PointCloudTrans::cloudCallback, this);

    // Publish a point cloud of filtered data that was not part of table
    filtered_pub_ = nh_.advertise< pcl::PointCloud<pcl::PointXYZRGB> >("block_output", 1);

    // // Publish a point cloud of data that was considered part of the plane
    plane_pub_ = nh_.advertise< pcl::PointCloud<pcl::PointXYZRGB> >("plane_output", 1);

    // Publish interactive markers for blocks
    block_pose_pub_ = nh_.advertise< geometry_msgs::PoseArray >("/", 1, true);

    // Publish the transformed pc
    transformed_pub_ = nh_.advertise< pcl::PointCloud<pcl::PointXYZRGB> >("transformed_pc", 1);

  }


  // Proccess the point clouds
  void cloudCallback( const sensor_msgs::PointCloud2ConstPtr& msg )
  {
    ROS_INFO_STREAM("Received callback");

    block_poses_.poses.clear();
    // Basic point cloud conversions ---------------------------------------------------------------

    // Convert from ROS to PCL
    pcl::PointCloud<pcl::PointXYZRGB> cloud;
    pcl::fromROSMsg(*msg, cloud);

    // Make new point cloud that is in our working frame
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_transformed(new pcl::PointCloud<pcl::PointXYZRGB>);

    // Transform to whatever frame we're working in, probably the arm's base frame, ie "base_link"
    // tf_listener_.waitForTransform(std::string(arm_link), cloud.header.frame_id, cloud.header.stamp, ros::Duration(1.0));
    tf_listener_.waitForTransform(std::string(arm_link), cloud.header.frame_id, ros::Time(0), ros::Duration(0.0));

    if(!pcl_ros::transformPointCloud(std::string(arm_link), cloud, *cloud_transformed, tf_listener_))
    {
      ROS_ERROR("Error converting to desired frame");
      return;
    }

    // Limit to things we think are roughly at the table height ------------------------------------
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_filteredZ(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::PassThrough<pcl::PointXYZRGB> pass;
    pass.setInputCloud(cloud_transformed);
    pass.setFilterFieldName("z");
    // pass.setFilterLimits(0.2, 1.5);
    pass.setFilterLimits(limit_min_z, limit_max_z);
    //pass.setFilterLimits(table_height - 0.01, table_height + block_size + 0.02); // DTC
    pass.filter(*cloud_filteredZ);


    // Limit to things in front of the robot ---------------------------------------------------
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_filteredX(new pcl::PointCloud<pcl::PointXYZRGB>);
    pass.setInputCloud(cloud_filteredZ);
    pass.setFilterFieldName("x");
    pass.setFilterLimits(limit_min_x, limit_max_x);
    pass.filter(*cloud_filteredX);

    // pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_filteredY(new pcl::PointCloud<pcl::PointXYZRGB>);
    // pass.setInputCloud(cloud_filteredX);
    // pass.setFilterFieldName("y");
    // pass.setFilterLimits(limit_min_y, limit_max_y);
    // pass.filter(*cloud_filteredY);

    // std::cerr << "Cloud before filtering: " << std::endl;
    // std::cerr << *cloud_transformed << std::endl;

    // pcl::io::savePCDFile("test_pao_desk.pcd", *cloud_transformed);
    // pcl::io::savePCDFileASCII("test_pao_desk.pcd", *cloud_transformed);
    // std::cout << "FILE SAVED!!!" << std::endl;

    // Create filtered point cloud: Statistical removal has slightly better resulst than the radius outlier.
    // both work equally slow
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::StatisticalOutlierRemoval<pcl::PointXYZRGB> sor;
    // sor.setInputCloud (cloud_transformed);
    sor.setInputCloud (cloud_filteredX);
    sor.setMeanK (meanK);
    sor.setStddevMulThresh (stdDev);
    sor.filter (*cloud_filtered);

    // ------------------------------------------------------

    // Organising the pc to then be able to put it through background removal
    cloud_filtered->width = 1920;
    cloud_filtered->height = 1080;
    cloud_filtered->is_dense = false;
    cloud_filtered->points.resize(cloud_filtered->height*cloud_filtered->width);

    transformed_pub_.publish(cloud_filtered);

    //---------------------------
    // if (cloud_filtered->height !=1)
    //   std::cerr << "Cloud before filtering: " << cloud_filtered->height<< std::endl;
    //   scene_segmentation.extractPlanes(inlier_points, radians_threshold, cm_distance_thr, cloud_filtered);
    //   filtered_pub_.publish(cloud_filtered);


    std::cout << "Extracting planes..." << std::endl;

    // estimate normals
    pcl::IntegralImageNormalEstimation<PointT, pcl::Normal> ne;
    pcl::PointCloud<pcl::Normal>::Ptr normal_cloud (new pcl::PointCloud<pcl::Normal>);
    ne.setMaxDepthChangeFactor (0.03f);
    ne.setNormalSmoothingSize (20.0f);
    ne.setNormalEstimationMethod (ne.COVARIANCE_MATRIX);
    ne.setInputCloud (cloud_filtered);
    ne.compute (*normal_cloud);

    // segment planes
    double mps_start = pcl::getTime ();
    pcl::OrganizedMultiPlaneSegmentation<PointT, pcl::Normal, pcl::Label> mps;
    std::vector<pcl::PlanarRegion<PointT>, Eigen::aligned_allocator<pcl::PlanarRegion<PointT> > > regions;
    std::vector<pcl::ModelCoefficients> model_coefficients;
    std::vector<pcl::PointIndices> inlier_indices;
    pcl::PointCloud<pcl::Label>::Ptr labels (new pcl::PointCloud<pcl::Label>);
    std::vector<pcl::PointIndices> label_indices;
    std::vector<pcl::PointIndices> boundary_indices;

    mps.setMinInliers (inlier_points); //50000
    mps.setAngularThreshold (radians_threshold); //3 degrees
    // mps.setAngularThreshold (0.017453 * 2.0); //3 degrees
    mps.setDistanceThreshold (cm_distance_thr); //2cm = 0.02
    mps.setInputNormals (normal_cloud);
    mps.setInputCloud (cloud_filtered);
    mps.segmentAndRefine (regions, model_coefficients, inlier_indices, labels, label_indices, boundary_indices);
    double mps_end = pcl::getTime ();

    // get data from the planes
    std::cout << "  - radians_threshold: " << radians_threshold << std::endl;
    std::cout << "  - planes detected: " << regions.size () << std::endl;
    std::vector<int> idx;
    detected_planes.resize(regions.size());
    for (size_t i = 0; i < inlier_indices.size(); ++i) {
      pcl::PointIndices::Ptr plane_indices(new pcl::PointIndices);
      *plane_indices = inlier_indices[i];
      idx.insert(idx.end(), plane_indices->indices.begin(), plane_indices->indices.end());

      pcl::PointCloud<PointT>::Ptr contour (new pcl::PointCloud<PointT>);
      contour->points = regions[i].getContour();
      detected_planes[i].contour = contour;
    }

    // remove planes from main cloud
    pcl::PointIndices::Ptr overall_indices(new pcl::PointIndices);
    overall_indices->indices = idx;
    pcl::PointCloud<PointT>::Ptr tmp_cloud(new pcl::PointCloud<PointT>);
    pcl::ExtractIndices<PointT> extract;
    extract.setInputCloud(cloud_filtered);
    extract.setIndices(overall_indices);
    extract.setNegative(true);
    extract.filter(*tmp_cloud);
    cloud_filtered = tmp_cloud;
    plane_pub_.publish(tmp_cloud); // with a red colour in rviz, where the red is what is NOT planes

    pcl::io::savePCDFile("test_pao_desk.pcd", *tmp_cloud);
    // ----- PUT THE CENTRE OF MASS HERE -----
    // pcl::CentroidPoint< PointT >::CentroidPoint();
    // CentroidPoint<pcl::PointXYZ> centroid;
    // centroid.get(tmp_cloud);
    // std::cout << "centre point for pc " << centroid << std::endl;
    //--------------------------

    return;

    // Check if any points remain
    if( cloud_filtered->points.size() == 0 )
    {
      ROS_ERROR("0 points left");
      return;
    }
    else
    {
      ROS_INFO("[block detection] Filtered, %d points left", (int) cloud_filtered->points.size());
    }

    // Segment components --------------------------------------------------------------------------
    pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers(new pcl::PointIndices);

    // Create the segmentation object for the planar model and set all the parameters
    pcl::SACSegmentation<pcl::PointXYZRGB> seg;
    seg.setOptimizeCoefficients(true);
    seg.setModelType(pcl::SACMODEL_PLANE);
    seg.setMethodType(pcl::SAC_RANSAC); // robustness estimator - RANSAC is simple
    // seg.setMaxIterations(200); // Paola removed this!!!
    seg.setDistanceThreshold(cm_distance_thr); //0.005 determines how close a point must be to the model in order to be considered an inlier

    // std::cerr << "THRESHOLD"<< cm_distance_thr << std::endl;

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_plane(new pcl::PointCloud<pcl::PointXYZRGB>());

    int nr_points = cloud_filtered->points.size();

    // Segment cloud until there are less than 30% of points left? not sure why this is necessary
    while(cloud_filtered->points.size() > 0.3 * nr_points)
    {

      // Segment the largest planar component from the remaining cloud (find the table)
      seg.setInputCloud(cloud_filtered);
      seg.segment(*inliers, *coefficients);

      if(inliers->indices.size() == 0)
      {
        ROS_ERROR("[block detection] Could not estimate a planar model for the given dataset.");
        return;
      }

      //std::cout << "Inliers: " << (inliers->indices.size()) << std::endl;

      // Extract the planar inliers from the input cloud
      pcl::ExtractIndices<pcl::PointXYZRGB> extract;
      extract.setInputCloud(cloud_filtered);
      extract.setIndices(inliers);
      extract.setNegative(false);

      // Write the planar inliers to disk
      extract.filter(*cloud_plane);
      std::cout << "PointCloud representing the planar component: " << cloud_plane->points.size() << " data points." << std::endl;

      // Remove the planar inliers, extract the rest
      extract.setNegative(true);
      extract.filter(*cloud_filtered);


      // Debug output - DTC
      // Show the contents of the inlier set, together with the estimated plane parameters, in ax+by+cz+d=0 form (general equation of a plane)
      std::cerr << "Model coefficients: " << coefficients->values[0] << " "
                << coefficients->values[1] << " "
                << coefficients->values[2] << " "
                << coefficients->values[3] << std::endl;

    }

    // DTC: Removed to make compatible with PCL 1.5
    // Creating the KdTree object for the search method of the extraction
    //pcl::KdTree<pcl::PointXYZRGB>::Ptr tree(new pcl::KdTreeFLANN<pcl::PointXYZRGB>);
    //tree->setInputCloud(cloud_filtered);

    std::vector<pcl::PointIndices> cluster_indices;
    pcl::EuclideanClusterExtraction<pcl::PointXYZRGB> ec;
    ec.setClusterTolerance(0.005);
    ec.setMinClusterSize(100);
    ec.setMaxClusterSize(25000);
    //ec.setSearchMethod(tree);
    ec.setInputCloud(cloud_filtered);
    ec.extract(cluster_indices);

    // Publish point cloud data
    // filtered_pub_.publish(cloud_filtered);
    // plane_pub_.publish(cloud_plane);



    // for each cluster, see if it is a block
    for(size_t c = 0; c < cluster_indices.size(); ++c)
    {
      // find the outer dimensions of the cluster
      float xmin = 0; float xmax = 0;
      float ymin = 0; float ymax = 0;
      float zmin = 0; float zmax = 0;
      for(size_t i = 0; i < cluster_indices[c].indices.size(); i++)
      {
        int j = cluster_indices[c].indices[i];
        float x = cloud_filtered->points[j].x;
        float y = cloud_filtered->points[j].y;
        float z = cloud_filtered->points[j].z;
        if(i == 0)
        {
          xmin = xmax = x;
          ymin = ymax = y;
          zmin = zmax = z;
        }
        else
        {
          xmin = std::min(xmin, x);
          xmax = std::max(xmax, x);
          ymin = std::min(ymin, y);
          ymax = std::max(ymax, y);
          zmin = std::min(zmin, z);
          zmax = std::max(zmax, z);
        }
      }

      // Check if these dimensions make sense for the block size specified
      float xside = xmax-xmin;
      float yside = ymax-ymin;
      float zside = zmax-zmin;

      const float tol = 0.01; // 1 cm error tolerance
      // In order to be part of the block, xside and yside must be between
      // blocksize and blocksize*sqrt(2)
      // z must be equal to or smaller than blocksize
      if(xside > block_size-tol &&
         xside < block_size*sqrt(2)+tol &&
                 yside > block_size-tol &&
         yside < block_size*sqrt(2)+tol &&
                 zside > tol && zside < block_size+tol)
      {
        // If so, then figure out the position and the orientation of the block
        float angle = atan(block_size/((xside+yside)/2));

        if(yside < block_size)
          angle = 0.0;

        ROS_INFO_STREAM("[block detection] xside: " << xside << " yside: " << yside << " zside " << zside << " angle: " << angle);
        // Then add it to our set
        addBlock( xmin+(xside)/2.0, ymin+(yside)/2.0, zmax - block_size/2.0, angle);
      }

      // return;
  }

    if(block_poses_.poses.size() > 0)
    {
      block_pose_pub_.publish(block_poses_);
      ROS_INFO("[block detection] Finished");
    }
    else
    {
      ROS_INFO("[block detection] Couldn't find any blocks this iteration!");
    }
}

  void addBlock(float x, float y, float z, float angle)
  {
    geometry_msgs::Pose block_pose;
    block_pose.position.x = x;
    block_pose.position.y = y;
    block_pose.position.z = z;

    Eigen::Quaternionf quat(Eigen::AngleAxis<float>(angle, Eigen::Vector3f(0,0,1)));

    block_pose.orientation.x = quat.x();
    block_pose.orientation.y = quat.y();
    block_pose.orientation.z = quat.z();
    block_pose.orientation.w = quat.w();

    // Discard noise
    if( block_pose.position.y > 10 || block_pose.position.y < -10 )
    {
      ROS_WARN_STREAM("Rejected block: " << block_pose );
    }

    ROS_INFO_STREAM("Added block: \n" << block_pose );

    block_poses_.poses.push_back(block_pose);
  }

};

};

void callback(point_cloud_transformation::point_cloud_transformationConfig &config, uint32_t level) {
  ROS_INFO("Reconfigure Request: %d %f",
            config.meanK, config.stdDev);
  meanK = config.meanK;
  limit_min_z = config.limit_min_z;
  limit_max_z = config.limit_max_z;
  limit_min_x = config.limit_min_x;
  limit_max_x = config.limit_max_x;
  limit_min_y = config.limit_min_x;
  limit_max_y = config.limit_max_x;

  inlier_points = config.inlier_points;
  radians_threshold = config.radians_threshold;
  cm_distance_thr = config.cm_distance_thr;
  // point_cloud_transformation::PointCloudTrans detector("pcl_lab");
}

int main(int argc, char** argv)
{

  ROS_INFO_STREAM("Starting Point Cloud Transformation node");

  ros::init(argc, argv, "point_cloud_transformation");

  // Roslaunch config parameters ----
  // ros::NodeHandle nh("~");
  // nh.getParam("/point_cloud_transformation/meanK", meanK);
  // nh.getParam("/point_cloud_transformation/stdDev", stdDev);
  // point_cloud_transformation::PointCloudTrans detector("pcl_lab");
  // Roslaunch config parameters ----

  // Dynamic reconfigure part -----
  dynamic_reconfigure::Server<point_cloud_transformation::point_cloud_transformationConfig> server;
  dynamic_reconfigure::Server<point_cloud_transformation::point_cloud_transformationConfig>::CallbackType f;
  f = boost::bind(&callback, _1, _2);
  server.setCallback(f);

  // Dynamic reconfigure part
  point_cloud_transformation::PointCloudTrans detector("pcl_lab");

  ros::spin();
  return 0;
}
