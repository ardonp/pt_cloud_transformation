cmake_minimum_required(VERSION 2.8.3)
project(point_cloud_transformation)

#find_package(Eigen REQUIRED)
#find_package(Boost REQUIRED thread date_time system filesystem program_options python)

# Load catkin and all dependencies required for this package
find_package(catkin REQUIRED
  roscpp
  std_msgs
  pcl_ros
  eigen_conversions
  tf
  tf_conversions
)

generate_dynamic_reconfigure_options(
  cfg/point_cloud_transformation.cfg
  #...
)

catkin_package(DEPENDS
  roscpp
  std_msgs
  pcl_ros
  eigen_conversions
  tf
  tf_conversions
#  INCLUDE_DIRS include
#  LIBRARIES ${PROJECT_NAME}
)

# Enable gdb to show line numbers
#SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -g")
set(CMAKE_BUILD_TYPE Debug)

## Build
include_directories(
  ${catkin_INCLUDE_DIRS}
  ./include/
)
link_directories(${catkin_LIBRARY_DIRS})

add_library(segmentation src/scene_segmentation.cpp)

add_executable(point_cloud_transformation src/point_cloud_transformation.cpp)
add_dependencies(point_cloud_transformation ${PROJECT_NAME}_gencfg)
target_link_libraries(point_cloud_transformation ${catkin_LIBRARIES})
target_link_libraries(point_cloud_transformation ${PCL_COMMON_LIBRARIES} ${PCL_IO_LIBRARIES} ${PCL_FILTERS_LIBRARIES})


# make sure configure headers are built before any node using them
#add_executable(point_cloud_transformation src/server.cpp)
#add_dependencies(point_cloud_transformation ${PROJECT_NAME}_gencfg)
#target_link_libraries(point_cloud_transformation ${catkin_LIBRARIES})
